package com.adobe.aem.training.core.servlets;

import com.adobe.aem.training.core.services.Activities;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

@Component(service = {Servlet.class})
@SlingServletResourceTypes(
        resourceTypes = "training/components/page",
        methods = HttpConstants.METHOD_POST,
        extensions = "json"
)
@ServiceDescription("Resource Type Servlet for Random Activity")
public class ActivitiesServlet extends SlingAllMethodsServlet {

    @Reference
    private transient Activities activitiesService;

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");

        // We're receiving one request parameter
        String firstName = request.getParameter("firstName");

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("random-activity", activitiesService.getRandomActivity());

        jsonObject.addProperty("first-name", firstName.toUpperCase());

        jsonObject.addProperty("response-from", "Via Resource Type Servlet");

        Gson gson = new Gson();
        out.println(gson.toJson(jsonObject));
        response.flushBuffer();
    }
}
