package com.adobe.aem.training.core.beans;

import java.util.List;

public class Card {

    private String cardImage;
    private String fullName;
    private String description;
    private List<String> foodItems;

    public Card(String cardImage, String fullName, String description, List<String> foodItems) {
        this.cardImage = cardImage;
        this.fullName = fullName;
        this.description = description;
        this.foodItems = foodItems;
    }

    public String getCardImage() {
        return cardImage;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getFoodItems() {
        return foodItems;
    }
}
